//
//  ViewController.swift
//  SO-32954762
//
//  Created by Abizer Nasir on 05/10/2015.
//  Copyright © 2015 Jungle Candy Software. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var quoteLabel: UILabel!

    let quotes = [
        "Hustle is the dark horse of creativity,\n the close cousin of Grit and Tenacity.\n Without the hustle, drive, and complete devotion to making things happen, you are average.\n\n- Rebecca Rebouché ",
        "What you want to do is not study in some prestigious field,\nbut study something that a prestigious field will grow out of.\n That’s the really big win.\n\n - Paul Graham",
        "There’s nothing like being tossed into necessity to help you figure out who you are and what matters most in life –\n necessity may be the mother of invention, but it’s even more so the fairy godmother of self-invention.\n\n - Maria Popova",
        "Pursuing that feeling of not really knowing what to do,and choosing what doesn’t quite seem like the logical next step, but feels right at a gut level, is how I’ve pieced together where I am today. It’s about that combination of anxiety about going into territory where I’m totally unfamiliar, and not knowing a big chunk of it.\n\n - Liz Danzico",
        "On the fringes...is where disruptive innovation begins.\n\n - Neri Oxman",
        "I wanted to be a certain kind of woman.\n I became that kind of woman.\n\n - Diane von Furstenburg"
    ]

    // These are just vector solid colours stored in the Asset Catalogue.
    let images = [
        "Image1",
        "Image2",
        "Image3",
        "Image4",
        "Image5",
        "Image6"
    ]


    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    @IBAction func onNext(sender: UIButton) {
        setupView()
    }

    private func randomImage() -> UIImage {
        let idx = Int(arc4random_uniform(UInt32(images.count)))
        guard let image = UIImage(named: images[idx]) else { fatalError() }

        return image
    }

    private func randomQuote() -> String {
        let idx = Int(arc4random_uniform(UInt32(quotes.count)))
        return quotes[idx]
    }

    private func setupView() {
        imageView.image = randomImage()
        quoteLabel.text = randomQuote()
    }

}

